// import AppService from './modules/app.service';
// import {config} from './modules/config';
import './modules/header.conponent'

//LIBRARIES CSS
import './assets/css/libraries/slick.css';
import './assets/css/libraries/swiper.min.css';
import 'owl.carousel/dist/assets/owl.carousel.css';
import './assets/css/libraries/ion.rangeSlider.min.css';


//LIBRARIES JS
import './js/libraries/slick.min';
import './js/libraries/swiper.min';
//FONTS
import './assets/css/material-design-iconic-font.css';

//SCRIPTS
import './js/universal-scripts';
import './js/common';
import './js/mobile-scripts';

//STYLES
import './assets/css/main.css';
import './assets/sass/main.sass';


